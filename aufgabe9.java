
public class AufgabeX {
	
	private static void main(String[] args) {
		
		System.out.println(fib(15));
		
	}
	
	public static int fib(int pos) {
		if(pos > 1)
		{
			return fib(pos-1) + fib(pos-2);
		}
		else if(pos == 1)
		{
			return 1;
		}
		else if(pos == 0)
		{
			return 0;
		}
		
		return -1;
	}

}
